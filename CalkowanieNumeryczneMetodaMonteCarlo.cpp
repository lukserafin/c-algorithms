#include <iostream>
#include <cstdlib>
#include <time.h>

using namespace std;

long double ZwrocWartoscFunkcji(int* wspolczynniki, int n, long double x);

int main()
{
	int ilosc, an, i, b, c, n;
	// Wczytujemy dane dotycz�ce ca�kowania, funkcje, przedzia�y.

	cout << "Podaj wspolczynnik przy n-tej potedze: " << endl;
	cin >> an;

	int* wspolczynniki = new int[an];

	for (i = 0; i <= an; i++)
	{
		if (i == 0) cout << "Podaj wartosc wyrazu wolnego: " << endl; else
			cout << "Podaj wspolczynnik " << i << "stopnia wielomianu" << endl;
		cin >> wspolczynniki[i];
	}

	cout << "Podaj poczatek przedzialu calkowania: " << endl;
	cin >> b;

	cout << "Podaj koniec przedzialu calkowania: " << endl;
	cin >> c;

	// Wypisanie pobranej funkcji
	cout << "Wprowadzony wielomian to: " << endl;
	for (i = an; i >= 0; i--)
	{
		if (i == 0) cout << wspolczynniki[i]; else
			cout << wspolczynniki[i] << "x^" << i;
		if (i > 0)
			if (wspolczynniki[i] >= 0) cout << "+"; else cout << "-";
	}

	cout << endl << "Podaj dokladnosc wyznaczania max/min funkcji: ";
	cin >> n;

	// Max i Min lokalne funkcji w podanych przedzia�ach warto�ci
	long double wartosc = 0, maximum = 0, minimum = 0, krok = 1.0 / n;

	// Wyliczamy wartosci funkcji w okreslonym przedziale, okreslamy minimum i maximum funkcji.
	for (double i = b; i <= c; i = i + krok)
	{
		wartosc = ZwrocWartoscFunkcji(wspolczynniki, an, i);

		if (wartosc > maximum) maximum = wartosc;

		if (wartosc < minimum) minimum = wartosc;

	}

	// Wyznaczamy wysokosc i szerokosc zakresu w ktorym bedziemy losowac.
	long double wysokosc, szerokosc;

	// Szerokosc to roznica przedzialu koncowego i poczatkowego.
	szerokosc = (long double)(c - b);

	// Wysokosc to roznica wartosci maxymalnej i minimalnej funkcji w przedziale.
	wysokosc = maximum - minimum;

	// Losowanie w punktow
	cout << "Podaj liczbe losowa�: " << endl << "Uwaga! Zakres od 1 do 2^32 - 1" << endl;
	cin >> ilosc;

	long double X = 0.0, Y = 0.0;
	int licznik = 0;

	for (i = 0; i < ilosc; i++)
	{
		X = ((long double)rand() / (RAND_MAX)) * (c - b) + b;
		Y = ((long double)rand() / (RAND_MAX)) * (wysokosc - szerokosc) + szerokosc;

		wartosc = ZwrocWartoscFunkcji(wspolczynniki, an, i);

		if (Y > 0 && wartosc > 0 && Y <= wartosc) licznik++;

		if (Y < 0 && wartosc < 0 && Y >= wartosc) licznik--;

	}

	// Wartosc calki
	long double P;

	P = (szerokosc * wysokosc) * (long double)licznik / (long double)ilosc;

	cout << "Punkty trafione: " << licznik << endl;
	cout << "Warto�� calki: " << P << endl;

	getchar();
	getchar();
	return 0;
}

// Obliczanie warto�ci funkcji w x
long double ZwrocWartoscFunkcji(int* wspolczynniki, int n, long double x) {
	long double funkcja = 0;

	for (int i = 0; i < n; i++)
		funkcja += ((long double)wspolczynniki[i]) * ((long double)pow(x, (double)(n - i - 1)));

	return funkcja;
}