#include<iostream>
#include<iomanip>
using namespace std;

double f(double x)
{
	//funkcja zawsze przyjmuje wartosci dodatnie
	//wi�c mo�na pomin�� wartos� bezwzgl�dn�
	return x * x + x + 2;
}

double Pole(int a, int b, int n)
{
	double h = (b - a) / (double)n; //wysokos� trapez�w
	double S = 0.0; //zmienna b�dzie przechowywa� sum� p�l trapez�w
	double podstawa_a = f(a), podstawa_b;

	for (int i = 1; i <= n; i++)
	{
		podstawa_b = f(a + h * i);
		S += (podstawa_a + podstawa_b);
		podstawa_a = podstawa_b;
	}
	return S * 0.5 * h;
}

int main()
{
	int a, b, n;
	cout << "Podaj przedzia� [a, b]\na = ";
	cin >> a;
	cout << "b = ";
	cin >> b;
	cout << "Podaj liczb� trapez�w: ";
	cin >> n;

	if (!(a < b))
		cout << "To nie jest przedzia�!";
	else
		cout << "Pole figury wynosi: " << fixed << setprecision(2) << Pole(a, b, n);


	cin.ignore();
	cin.get();
	return 0;
}