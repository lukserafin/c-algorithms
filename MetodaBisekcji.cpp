#include <iostream>
#include<iomanip>
using namespace std;

double a, b, eps, s;

double f(double x)
{
	//rozpatrujemy wielomian f(x) = x^3 - 3x^2 + 2x - 6 
	return x * (x * (x - 3) + 2) - 6; //rozbijam schematem Hornera
}

double polowieniePrzedzialow(double a, double b){
	if (f(a) == 0.0) {
		return a;
	}
	else if (f(b) == 0.0) {

		return b;
	}

	else {

		double eps = 0.00001;
		double s;

		while (b - a > eps)
		{
			s = (a + b) / 2;

			if (f(s) == 0.0) //jesli miejsce zerowe jest w srodku 
				return s;

			if (f(a) * f(s) < 0)
				b = s;
			else
				a = s;
		}
		return (a + b) / 2;
	}
}


int main() {
	cout << "----------Algorytm wyznaczania miejsca zerowego funkcji----------" << endl; 
	
	// Wprowadzenie zakresu funkcji

	cout << "Podaj zakres poczatkowy przedzialu funkcji: ";
	cin >> a;
	cout << "Podaj zakres koncowy przedzialu funkcji: ";
	cin >> b;
	if (a < b) {
		cout << "Podany zakres funkcji wynosi [" << a << "," << b << "]" << endl;

		cout << fixed << setprecision(5) <<"Znalezione miejsce zerowe wynosi: ";
		cout << polowieniePrzedzialow(a, b);
	}
	else {
		cout << "Podany zakres funkcji jest nieprawidłowy" << endl;
	}
	
	system ("PAUSE");
}


