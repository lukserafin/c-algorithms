#include <iostream>

using namespace std;

int tablica[20], n, k, j, min;

int main()
{
	cout << "Podaj ilosc elementow: " << endl;
	cin >> n;

	for (int i = 1; i <= n; i++)
	{
		cout << "Element " << i << ": ";
		cin >> tablica[i];
	}

	for (int i = 2; i <= n; i++)
	{
		j = i;
		k = tablica[j];

		while ((tablica[j - 1] > k) && (j > 1))
		{
			tablica[j] = tablica[j - 1];
			j--;
		}
		tablica[j] = k;
	}

	for (int i = 1; i <= n; i++)
	{
		cout << tablica[i] << endl;
	}
	system("PAUSE");
	return 0;
}