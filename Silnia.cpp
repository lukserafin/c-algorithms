#include<iostream>
using namespace std;

int main() {

	cout << "Program oblicza silnie liczby calkowitej n, 0 <= n <= 170." << endl;
	cout << "Podaj n = ";

	// Wczytaj n z klawiatury
	int n;
	cin >> n;

	// Je�eli n>=0 i n<=170 to oblicz n!
	// W przeciwnym wypadku wypisz "Liczba spoza zakresu!"    

	if (n >= 0 && n <= 170) {
		// Obliczanie n! gdy n<0 jest niedozwolone
		// 171! przekracza typ double

		// Oblicz n!
		double result; // Zmienna do przechowywania wyniku
		// Zastosowali�my typ double, �eby rozszerzy� zakres wynik�w

		// je�eli n=0 to wynik = 1
		if (n == 0) {
			result = 1;
		}
		else {
			// n r�ne od 0, wi�c liczymy 1*2*3*....n
			result = 1;
			// p�tla od i=1 do n
			for (int i = 1; i <= n; i++) {
				result = result * i;
			}
		}

		// Wypisz wynik
		cout << "Wynik = " << result << endl;
	}
	else {
		cout << "Liczba spoza zakresu!" << endl;
	}

	system("PAUSE");
	return 0;
}
