#include <iostream>
#include <ctime>

using namespace std;

void qsort(int T[], int d, int g);
int podziel(int T[], int d, int g);

int main() {

	int n;
	cout << "Podaj liczbe elementow n:";
	cin >> n;
	int* T = new int[n];
	srand(time(NULL));

	for (int i = 0; i < n; i++) {
		T[i] = rand(); 
	}
	cout << "Nieposortowana tablica: ";
	for (int i = 0; i < n; i++) {
		cout << T[i] << ", ";
	}

	qsort(T, 0, n - 1);

	cout << "\nPosortowana tablica: ";
	for (int i = 0; i < n; i++) {
		cout << T[i] << ", ";
	}

	system("PAUSE");
}

int podziel(int T[], int d, int g) {
	int s = d;
	int e_gran = T[d];

	for (int i = d + 1; i <= g; i++) {
		if (T[i] < e_gran) {
			s++;
			int tmp = T[s];
			T[s] = T[i];
			T[i] = tmp;
		}
		int temp = T[d];
		T[d] = T[s];
		T[s] = temp;
	}
	return s;
}

void qsort(int T[], int d, int g) {
	int q;
	if (d < g) {
		q = podziel(T, d, g);
		qsort(T, d, q);
		qsort(T, q + 1, g);
	}
}