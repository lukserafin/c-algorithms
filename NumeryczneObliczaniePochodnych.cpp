#include <iostream>
using namespace std;

double x;
double h = 0.001;

double f(double x)
{
	//rozpatrujemy wielomian f(x) = - 3x^2 + 1 
	return x * (x - 3) + 1; //rozbijam schematem Hornera
}

double p1f(double x, double h) //f'(x) = [f(x+h)-f(x-h)]/(2*h)
{
	return(f(x + h) - f(x - h)) / (2.0 * h);
}

double p2f(double x, double h) //f''(x) = [f(x+h)+f(x-h)-2*f(x)]/(h*h)
{
	return(f(x + h) + f(x - h)-2*f(x)) / (h * h);
}

int main() {

	cout << "Podaj x: ";
	cin >> x;

	cout << "Podaj h: ";
	cin >> x;

	cout << "Wynik pochodnej pierwszego rz�du f'(x):" << p1f(x,h) << endl;
	cout << "Wynik pochodnej pierwszego rz�du f''(x):" << p2f(x,h) << endl;
	
	system("PAUSE");
}