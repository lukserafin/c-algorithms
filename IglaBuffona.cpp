#include <iostream>
#include <time.h>  

using namespace std;

int main() {
	cout << "Podaj dlugosc igly: " << endl;
	double l;
	cin >> l;
	cout << "Podaj szerokosc miedzy liniami: " << endl;
	double d;
	cin >> d;
	cout << "Podaj liczbe prob: " << endl;
	double n;
	cin >> n;
	double p = 0;

	
	srand(time(NULL));

	if (n <= 0) {
		cout << "Niepoprawne dane. Liczba prob nie moze byc mniejsza od 0. Wpisz ponownie dane." << endl;
		return 1;
	}
	else if (l > d) {
		cout << "Niepoprawne dane. Dlugosc igly nie moze byc wieksza niz odleglosc miedzy liniami. Wpisz ponownie dane." << endl;
		return 1;
	}
	else {

		for (int i = 0; i < n; i++) {

			double x; //Kat miedzy igla a normalna do linii
			x = (double)rand() / RAND_MAX * 3.14159265359 / 2;
			double z; //Odleglosc srodka ig�y od najbli�szej linii
			z = ((double)rand() / RAND_MAX) * d / 2;		
			if (z <= ((l / 2) * sin(x))) {
				p = p + 1;
			}

		}

		double pi = (n * 2 * l) / (d * p);
		cout << "Wynik: "<< pi << endl;

	}

	system("PAUSE");
	return 0;
}