
#include <iostream>
#include <ctime>

using namespace std; 

void qsort(int T[], int d, int g);


int main() {

    int n;
    cout << "Podaj liczbe elementow n:";
    cin >> n;
    int* T = new int[n];
    srand(time(NULL));

    for (int i = 0; i < n; i++) {
        T[i] = rand(); 
    }
    cout << "Nieposortowana tablica: ";
    for (int i = 0; i < n; i++) {
        cout << T[i] << ", ";
    }

    cout << "\n----------------------------------\n";

    qsort(T, 0, n - 1);

    cout << "Posortowana tablica: ";
    for (int i = 0; i < n; i++) {
        cout << T[i] << ", ";
    }

    system("PAUSE");
}



void qsort(int T[], int d, int g)
{
    int i = d;
    int j = g;
    int x = T[(d + g) / 2];
    do
    {
        while (T[i] < x)
            i++;

        while (T[j] > x)
            j--;

        if (i <= j)
        {
            swap(T[i], T[j]);

            i++;
            j--;
        }
    } while (i <= j);

    if (d < j) qsort(T, d, j);

    if (g > i) qsort(T, i, g);

}