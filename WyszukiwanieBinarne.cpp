#include <iostream>

using namespace std;

int main() {
	int T[6] = { 1,2,3,4,5,6 };
	int liczba, n = 6, l, p, s;
	
	//Wyswietlenie posortowanej tablicy
	cout << "Zawartosc tablicy:\n";
	for (int i = 0; i < n; i++)
		cout << "T [" << i + 1 << "] = " << T[i] << endl;
	
	//Okre�lenie szukanej liczby
	cout << "Podaj szukana liczbe: ";
	cin >> liczba;
	l = 0;
	p = n - 1;
	while (true)
	{
		if (l > p)
		{
			cout << "Nie odnaleziono szukanego elementu" << endl;
			break;
		}

		//Obliczamy warto�� �rodka przedzia�u
			s = (l + p) / 2;
		//Sprawdzenie czy �rodek przedzia�u jest szukan� liczba
		if (T[s] == liczba)
		{
			cout << "Odnaleziono liczbe " << liczba << " pod indeksem " << s + 1 << endl;
			break;
		}
		else if (T[s] < liczba)
			l = s + 1;
		else
			p = s - 1;
	}
	return 0;
}
