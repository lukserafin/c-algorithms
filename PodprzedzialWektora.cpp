#include <iostream>
#include <algorithm>

using namespace std;


int main() {

	cout << "Szukanie najwiekszej sumy w ciaglym podprzedziale wektora" << endl;

	int T[10] = { 31,-41,59,26,-53,58,97,-93,-23,84 };
	int n = 9;

	//wyswietlenie wektora wejsciowego
	cout << "\nWketor wejsciowy: " << endl;
	for (int i = 0; i <= n; i++) {
		cout << T[i] << " ";

	}

	int maxsofar = 0;
	int maxhere = 0;

	int dlugosc_podprzedzialu = 0;
	int	koniec_podprzedzialu=0;
	int poczatek_podprzedzialu;


	for (int i = 0; i < n; i++) {

		maxhere = max(0, maxhere + T[i]);

		//maxsofar = max(maxhere, maxsofar);
		if (maxsofar < maxhere) {
			maxsofar = maxhere;
			dlugosc_podprzedzialu ++;
			koniec_podprzedzialu = i;

		}
		poczatek_podprzedzialu = koniec_podprzedzialu - dlugosc_podprzedzialu + 1;
	}

	

	cout << "\nMaksymalna suma w ciaglym podprzedziale wektora wynosi " << maxsofar << endl;
	cout << "Indeksy podprzedzialu to: [" << poczatek_podprzedzialu <<","<<koniec_podprzedzialu<<"]"<< endl;
	
	
	
	system("PAUSE");
}



